- name: Sales Pipeline Coverage
  base_path: "/handbook/sales/performance-indicators/"
  definition: IACV of pipeline with close dates in a given period (quarter) divided by IACV target.
  target: On day one of the current quarter, total pipeline coverage should be 2.4X, total pipeline for the following quarter should be 2.2X, and total pipeline for 2 quarters out should be 1.0X. On day one of the current quarter, stage 3+ pipeline coverage should be 1.5X and stage 3+ pipeline for the following quarter should be 0.8X.
  org: Sales
  is_key: true
  public: true
  health:
    level: 0
    reasons: 
    - to be updated
- name: Percent of Ramping Reps at or Above 70% of Quota
  base_path: "/handbook/sales/performance-indicators/"
  definition: A Rep is considered ramping if they have a tenure of less than or equal
    to 12 months in that particular role at GitLab. The percentage of attainment calculation
    is the the quota divided by the gross IACV from closed won opportunities in a
    particular month.
  target: greater than 70%
  org: Sales
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - to be updated
  sisense_data:
    chart: 8726882
    dashboard: 446004
    embed: v2
- name: Percent of Ramped Reps at or Above Quota
  base_path: "/handbook/sales/performance-indicators/"
  definition: A Rep is considered ramped if they have a tenure greater than 12 months
    in that particular role at GitLab. The percentage of attainment calculation is
    the the quota divided by the gross IACV from closed won opportunities in a particular
    month. Industry average is 45%-65% of reps achieving 100% of quota.
  target: greater than 55%
  org: Sales
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - to be updated
  sisense_data:
    chart: 8726847
    dashboard: 446004
    embed: v2
- name: Win Rate
  base_path: "/handbook/sales/performance-indicators/"
  definition: For a given month (using opportunity close date), the win rate is calculated as the count of closed won sales-assisted, new first order opportunities divided by the total count of closed sales-assisted, new first order opportunities.
  target: 60% for SMB, 50% for Mid-Market, 45% for Large
  org: Sales
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - to be updated
  sisense_data:
    chart: 10276510
    dashboard: 446004
    embed: v2
- name: New ARR
  base_path: "/handbook/sales/performance-indicators/"
  definition: ARR for a customer's first paying month.
  target: Monthly target set by Finance
  org: Sales
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - to be updated
  sisense_data:
    chart: 
    dashboard: 
    embed: 
- name: New Logos
  base_path: "/handbook/sales/performance-indicators/"
  definition: Count of new customers by month
  target: Monthly targets set by Sales & Marketing
  org: Sales
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - to be updated
  sisense_data:
    chart: 
    dashboard: 
    embed:
- name: IACV vs Plan
  base_path: "/handbook/sales/performance-indicators/"
  definition: The year-to-date cumulative sum of IACV divided by the year-to-date
    plan for IACV. Details on IACV can be found in the sales <a href="https://about.gitlab.com/handbook/sales/#incremental-annual-contract-value-iacv">handbook
    page</a>.
  target: greater than 1
  org: Sales
  is_key: true
  public: false
  health:
    level: 3
    reasons:
    - 106% of target for Sept
  urls:
  - https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6933430&udv=0
- name: ProServe Revenue vs Cost
  base_path: "/handbook/sales/performance-indicators/"
  definition: For a given month, revenue from professional services divided by professional
    services operating expenses. Details on professional services revenue can be found
    in the sales <a href="https://about.gitlab.com/handbook/sales/#proserve-contract-value-pcv">handbook
    page</a>.
  target: greater than 1.1
  org: Sales
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - 2.23 against a target of 1.1
  sisense_data:
    chart: 6931495
    dashboard: 446004
    embed: v2
- name: ProServe Deal and Dollar Attach Rate
  base_path: "/handbook/sales/performance-indicators/"
  definition: Deal Attach Rate is the number of Professional Services closed won opportunities divided by the number of total closed won opportunities during a given month (by opportunity close date). Dollar Attach Rate is the Professional Services Value of closed won opportunities divided by the IACV of closed won opportunities during a given month (by opportunity close date).
  target: 13% for Deal Attach Rate, 6% for Dollar Attach Rate
  org: Sales
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - to be updated
  sisense_data:
    chart: 
    dashboard: 
    embed: 
- name: Time to First Value
  base_path: "/handbook/sales/performance-indicators/"
  definition: Number of days from the original contract start date for a customer to activate at least 10% of their purchased licenses.
  target: 30 days
  org: Sales
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - to be updated
  sisense_data:
    chart: 
    dashboard: 
    embed: 
- name: Total Renewal Rate
  base_path: "/handbook/sales/performance-indicators/"
  definition: Dollars that renewed (at any time) divided by dollars that were supposed to renew in a given month.
  target: to be determined
  org: Sales
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - to be updated
  sisense_data:
    chart: 
    dashboard: 
    embed:
- name: On Time Renewal Rate
  base_path: "/handbook/sales/performance-indicators/"
  definition: Dollars that renewed in a given month divided by dollars that were supposed to renew in that month.
  target: to be determined
  org: Sales
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - to be updated
  sisense_data:
    chart: 
    dashboard: 
    embed:
- name: Net Retention
  base_path: "/handbook/sales/performance-indicators/"
  definition: Net Retention Calculation is extensively explained in the <a href="https://about.gitlab.com/handbook/customer-success/vision/#retention-and-reasons-for-churn">Customer
    Success's Vision page</a>.
  target: Above 130%
  org: Sales
  is_key: true
  public: false
  health:
    level: 3
    reasons:
    - 99% of target for Sept
  urls:
  - https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6055135&udv=0
- name: CAC Ratio
  base_path: "/handbook/sales/performance-indicators/"
  definition: Sales and Marketing expenses (including the cost of free users of gitlab.com) over trailing twelve months divided by current quarter ARR annualized minus ARR from the same period in the prior year annualized. Details on CAC can be found in the sales <a href="https://about.gitlab.com/handbook/sales/sales-term-glossary/#customer-acquisition-cost-cac">handbook
    page</a>.
  target: Monthly target set by Finance
  org: Sales
  is_key: true
  public: false
  health:
    level: 0
    reasons:
    - to be updated
  urls:
  - https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6933430&udv=0
- name: Self-serve Sales Ratio (deal and dollar)
  base_path: "/handbook/sales/performance-indicators/"
  definition: For a given month, the self-serve sales ratio (deal count) is calculated as the count of closed won web-direct opportunities divided by the count of all closed won opportunities. The self-serve sales ratio (IACV) is calculated as the net IACV of closed won web-direct opportunities divided by the net IACV of all closed won opportunities.
  target: greater than 10% (IACV), greater than 60% (deal count)
  org: Sales
  is_key: true
  public: true
  health:
    level: 1
    reasons:
    - to be updated
  sisense_data:
    chart: 6168683
    dashboard: 446004
    embed: v2
- name: New Hire Location Factor
  base_path: "/handbook/sales/performance-indicators/"
  definition: The <a href="https://about.gitlab.com/handbook/people-group/people-operations-metrics/#average-location-factor">average
    location factor</a> of all newly hired team members within the last rolling 3
    month as of the end of the period.
  target: less than 0.72
  org: Sales
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - .818 against a target of .72
  sisense_data:
    chart: 6450216
    dashboard: 446004
    embed: v2
- name: Sales Capacity
  base_path: "/handbook/sales/performance-indicators/"
  definition: Total number of sales reps adjusted for their tenure
  target: Monthly targets set by Sales
  org: Sales
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - to be updated
  sisense_data:
    chart: 
    dashboard: 
    embed: 
- name: Licensed User Growth
  base_path: "/handbook/sales/performance-indicators/"
  definition: The increase or decrease in contracted users on active paid subscriptions from one period of time to the next. Excludes OSS, 
    Education, Core and other non-paid users. The data source is Zuora.
  target: 3% increase per month
  org: Sales
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - to be updated
  sisense_data:
    chart: 10276547
    dashboard: 446004
    embed: v2
- name: ARPU
  base_path: "/handbook/sales/performance-indicators/"
  definition: The number of contracted users on active paid subscriptions. Excludes
    OSS, Education, Core and other non-paid users. The data source is Zuora.
  target: 2% increase per month
  org: Sales
  is_key: true
  public: false
  health:
    level: 0
    reasons:
    - to be updated
  urls:
  - https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6936923&udv=0
- name: ARR YoY
  base_path: "/handbook/sales/performance-indicators/"
  definition: ARR for a given month divided by ARR twelve months prior. Details on
    ARR can be found in the sales <a href="https://about.gitlab.com/handbook/sales/#annual-recurring-revenue-arr">handbook
    page</a>.
  target: Monthly target set by Finance
  org: Sales
  is_key: true
  public: false
  health:
    level: 0
    reasons:
    - to be updated
  urls:
  - https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6933786&udv=0

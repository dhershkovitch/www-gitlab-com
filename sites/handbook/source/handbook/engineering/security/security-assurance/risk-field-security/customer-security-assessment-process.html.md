---
layout: handbook-page-toc
title: "GitLab's Customer Assurance Activities"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose
It's no surprise that GitLab Customers and Prospects conduct Security due diligence activities prior to contracting with GitLab. We recognize the importance of these reviews and have designed this procedure to document the intake, tracking and responses to these activities.

## Scope
The Customer Assurance Activities Procedure is applicable to Security due dilligence reviews conducted by GitLab Customers and Prospects. This includes, but is not limited to, requests for [GitLab's Customer Assurance Package](/handbook/engineering/security/security-assurance/risk-field-security/customer-assurance-package.html), Contract Reviews, Customer Meetings, External Evidence (such as a SOC2 or Penetration Test), or Security Questionnaires. 

## Roles and Responsibilities

| Role | Responsibility |
| ------ | ------ |
| **Risk and Field Security team** | Maintain a mechanism to intake and respond to Customer Assurance Activities|
| | Provide complete and accurate responses within documented SLA|
| | Document and report any risk or trends identified during Customer Assurance Activities |
| | Maintain Metrics for [Security KPI](/handbook/engineering/security/performance-indicators/#security-impact-on-iacv)
| **Account Owner** | Recieve Customer or Prospect requests and submit to Risk and Field Security team |
| | Engage Solutions Architect or Technical Account Manager, as needed |
| | Provide information and documentation back to Customer or Prospect |
| **Solutions Architect** | Complete the First Pass, as applicable |
| **Current Customers** | Utilize Customer Self-Service tools noted below |
| |Contact your [Account Owner](/handbook/sales/#initial-account-owner---based-on-segment) at GitLab. If you don't know who that is, please reach out to [support@gitlab.com](mailto:support@gitlab.com) and ask to be connected to your Account Owner. |
| **Prospective Customers** | [Fill out a request](https://about.gitlab.com/sales/) and a representative will reach out to you. |

## Customer Assurance Activities Workflow

```mermaid
graph TD
subgraph Legend
	Link[These are clickable boxes]
	end
	A[Customer/Prospect:<br/>Review Self Service]--> B{Remaining Questions?};
  B--> C[No] & D[Yes]
  C --> Thanks!
  D --> E[Customer/Prospect:<br/>Contact GitLab]
  E--> F[GitLab Team Member:<br/>Complete First Pass];
  F--> G{Remaining Questions?}
  G--> H[No] & I[Yes]
  H --> K[Thanks!]
  I--> L[GitLab Team Member:<br/>Initiate Slack Request]
  L--> |Status: In Progress| O{R&FS: Complete Assurance Activities}
  O -->|CAP| P[R&FS: Reply to Account Owner]
  O-->|Contract Review| P
  O-->|Meeting| P
  O-->|External Evidence| P
  O-->|Questionnaire| P
  P --> |Status: Completed| Q[R&FS:<br/>Close Audit]

  click A "https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/customer-security-assessment-process.html#customer-self-service-information-gathering" "Customer Self-Service Information Gathering"
  click F "https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/customer-security-assessment-process.html#gitlab-assisted-information-gathering" "GitLab Assisted Information Gathering"
  click O "https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/customer-security-assessment-process.html#gitlab-assisted-information-gathering" "Risk and Field Security Support"
  
		style A stroke:#6e49cb, stroke-width:2px 
    style F stroke:#6e49cb, stroke-width:2px 
    style O stroke:#6e49cb, stroke-width:2px 
    style Link stroke:#6e49cb, stroke-width:2px

  ````

## Customer Self-Service Information Gathering

At GitLab, we are extremely [transparent](/handbook/values/#transparency) and the answers to many common questions may already be publicly available. However, we also [iterate](/handbook/values/#iteration) quickly, meaning the answers may change over time. We encourage our Customers and Prospects to utilize the below self-service resources as a first step in the process of assessing GitLab. 

* Search for [General Information about GitLab](https://about.gitlab.com) in our public handbook.
* Review [GitLab's Customer Assurance Package](/handbook/engineering/security/security-assurance/risk-field-security/customer-assurance-package.html)
* Review [GitLab's Product Security Documentation](https://docs.gitlab.com)

Should these resources not provide you with sufficient information, your Account Owner will follow the GitLab Assisted Information Gathering Process below. 

## GitLab Assisted Information Gathering
Despite GitLab's extereme transparency, there may be items our Customers and Prospects can't find using our Self Service resources above. Account Owners will facilitate these requests using the procedure below. 

1. The Risk and Field Security team mantains an **internal** database of commonly asked questions, the [**GitLab AnswerBase**](/handbook/engineering/security/security-assurance/risk-field-security/common-security-questions.html). We ask that prior to submitting a new request, GitLab team members utilize this tool and answer any questions they can. If the answer can't be found in the **GitLab AnswerBase**, GitLab Team Members can:
   * Pose a new question in GitLab AnswerBase
   * Pose a new question in the [#sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70) Slack Channel. 
   * Search for prior questions and answers in Slack using the following format: `[keywords] in:[channel name]`

2. If the request has a document to complete or review, GitLab team members load the file into the [Security Questionnaire Folder ](https://drive.google.com/open?id=0B6GNv2pwhtCxWVJWdEZCTUEwbXc) and ensure that the [Risk and Field Security team](/handbook/engineering/security/security-assurance/risk-field-security/#makeup-of-the-team) has edit access to the file.

3. Using the Customer Assurance Activities workflow in the [#sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70) Slack Channel, GitLab team members submit a request. The data requested in this workflow is **critical** for measuring our [Security KPI](/handbook/engineering/security/performance-indicators/#security-impact-on-iacv) and department goals. 
   * **Customer or Prospect Name**: the account name in SFDC
   * **Request Type**: Select from: 
     * GitLab's Customer Assurance Package
     * Contract Review
     * Customer Meeting
     * External Evidence (such as a SOC2 or Penetration Test)
     * Security Questionnaire
   * **Market or Indusrty Vertical**: the "indusrty" in the Account Details section of SFDC. These values tie dirctly to SiSence filters.
   * **Customer or Prospect Size**: the "sales segment" in the Sales Territory section of SFDC. These values tie dirctly to SiSence filters.
   * **Link to SFDC Opportunity**
   * **IACV/Revenue Impact of the Opportunity**
   * **Product Host:** Select from:
     * SaaS
     * Self-Managed
     * Others 
   * **Additional GitLabe Team Members**
   * **Due Date: **This is a free text box due to Slack restrictions. The date shoudl be formatted as **YYYY-MM-DD** and inline with the 10 business day SLA. 
   * **Other:** Include any relevant information such as specific areas of concern, URL reference to a GitLab issue, or other relevant information to support the team. **Note**: For SOC2 requests, you **MUST** include the Name, Title and Email of the recipient. 

An automated work flow will take the data from the Slackbot and create a new Audit in ZenGRC. 

## Risk and Field Security Support
* Customer Assurance Activities are assigned within ZenGRC. R&FS team members will use the "reply to thread" option in Slack to communicate with the requester.  
* Complete the requested Customer Assurance Activities using the [**Internal Only** Customer Questions Standards](https://docs.google.com/document/d/1y5qP1X2ITUjWBJxoRpGwRPMZC-CHAODoTio15cC7DhY/edit?usp=sharing) within GitLab's third party GRC Application. 
* Provide completed documentation to the requestor

**Important NOTE**: Follow-on questions greater than 5 questions are considered a new questionnaire and the SLA will reset.

## Service Level Agreements
Once the request is submitted via Slack, the Risk and Field Security will complete the activities within **10 business days**. 

## Exceptions
If the Account Owner or Customer Success point of contact feel they have sufficient knowledge and resoures to complete a Customer Assessment, this procedure does not have to used. These excpetions, will not be tracked. 

## References
Not applicable

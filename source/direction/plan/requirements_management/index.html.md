---
layout: markdown_page
title: "Category Direction - Requirements Management"
description: View the GitLab the category strategy for Requirements Management, part of the Plan stage's Requirements Management group. Learn more!
canonical_path: "/direction/plan/requirements_management/"
---

- TOC
{:toc}

## Requirements Management
<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. -->

|                       |                                 |
| -                     | -                               |
| Stage                 | [Plan](/direction/plan/)        |
| Maturity              | [Minimal](/direction/maturity/) |
| Content Last Reviewed | `2020-10-10`                    |


### Overview

Requirements Management enables documenting, tracing, and control of changes to agreed-upon requirements in a system. Our strategy is to make it simple and intuitive to create and trace your requirements throughout the entire Software DevOps lifecycle. 

We believe we can reduce the friction associated with managing requirements by tying it directly into the tools that a team uses to plan, create, integrate, and deploy their products. This can also provide real-time traceability and remove the need to track requirements across many disparate tools. 

#### What is Requirements Management?

It is often necessary to specify behaviors for a system or application. Requirements Management is a process by which these behaviors would be captured so that there is a clearly defined scope of work. A good general overview is provided in an [article from PMI](https://www.pmi.org/learning/library/requirements-management-planning-for-success-9669). For less restrictive environments, Requirements Management can take the form of jobs to be done (JTBD) statements, which are satisfied through iterative improvements or additional features.

Requirements management tools are often prescriptive in their process, requiring users to modify their workflows to include traceability. Our goal is to allow for such rigid process where required, but remove these barriers for organizations looking to achieve the process improvements offered by working with requirements in a less formal manner.

#### Aerospace Use Case

Regulated industries often have specific standards which define their development life-cycle. For example, commercial software-based aerospace systems must adhere to [RTCA DO-178C, Software Considerations in Airborne Systems and Equipment Certification](https://en.wikipedia.org/wiki/DO-178C). While this document covers all phases of the software development life cycle, the concept of traceability (defined as a documented connection) is utilized throughout. This connection must exist between the certification artifacts.

The most common trace paths needed are as follows:

* Software Allocated System Level Requirements <- High Level Software Requirements (HLR) <- Low Level Software Requirements (LLR) / Software Design <- Source Code <- Executable Object Code
* Software High Level & Low Level Requirements <- Test Cases <- Test Procedures <- Test Results

It is important to recognize that all artifacts must be under revision control.

During audits, teams are asked to demonstrate traceability from the customer specification through all downstream, version-controlled artifacts. Teams are often asked to analyze a change in a system level requirement, assessing exactly which downstream artifacts will need to be modified based on that change.

#### Other Regulated Industries

Further research has shown that many other regulated industries have similar process requirements. Medical, financial, and automative industries are held to similar standards as their aerospace counterparts.

#### Key Terms / Concepts

**Traceability** - The ability to link requirements to other requirements (both higher level and lower level), design, source code, or verification tests.

**Requirements Decomposition** - It is up to the developers and architects to decompose (break down) high level requirements into many smaller low level requirements. All of these decomposed requirements would generally trace up to the high level requirement, thus forming a one-to-many (HLR to LLR) relationship.

**Derived Requirements** - Because regulated industries often require that all functionality within the software trace to a requirement, it is often necessary to create requirements at the LLR / Design level. These requirements, that were not decomposed from a higher level requirement, are called Derived Requirements.

**Traceability Matrix** - A common artifact that is often required is a traceability matrix. This is a released document which shows all traceability links in the system / sub-system.


### What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

We recognize that we cannot immediately replace complex system level requirements management tools given their industry adoption and their extensive analysis capabilities which are well suited to managing very large system / subsystem requirement decomposition. We are therefore focusing on the largest pain point for our users based on extensive user research - tracing software implementation and verification to requirements in an automated manner.

We recognize that GitLab is in a unique position to deliver an integrated Requirements Management solution since linking to all aspects (version controlled files, test procedures, etc...) could be accomplished without the need for external tools. This would allow our solution to effectively link to all necessary artifacts within a single product offering.

With this in mind, we are planning embrace this vision by move the Requirements Management category towards [viable maturity](https://gitlab.com/groups/gitlab-org/-/epics/1697). We believe that the once we achieve this maturity, software teams will be able to bring existing requirements into GitLab, develop software and testing using the most efficient agile methodologies, natively capture all required traceability, and then export the resulting information for consumption by their customer or higher level system integration team.

- [Import Requirements](https://gitlab.com/groups/gitlab-org/-/epics/4347)
- [Export Requirements](https://gitlab.com/groups/gitlab-org/-/epics/4672)
- [Capture interactions with Requirements](https://gitlab.com/gitlab-org/product-analytics/-/issues/370)
- [Results of User Research](https://gitlab.com/groups/gitlab-org/-/epics/3708) - Finalize the results of the user research into actionable items.
- [Improve requirement management user experience](https://gitlab.com/groups/gitlab-org/-/epics/2898) - Continually improve the user experience surrounding requirements management.

The below image illustrates an initial node diagram of the requirements definition.

![Requirements Node Diagram](requirements_mind_map.png)

### Long term goals

Once we have made requirements management a great option for single software teams, we plan to continue iterating as follows.

* As GitLab expands to support larger enterprises, it is natural that there would be a need multiple levels of requirements such as group level requirements which are decomposed down to project level requirements. Our goal is to expand requirements to the group level, where they can be traced down to multiple project. This would allow for teams to create a system of sub-systems and perform all requirement tracing directly within GitLab.
* We recognize that requirements and their associated trace data is often required as release evidence / artifacts. We would like to work closely with our release team to integrate requirements traceability into release evidence.
* Visual representation of traceability and test coverage. We would like to provide a visual representation of ancestors and descendants of requirements, making it easy to visualize decomposition and traceability. It would also be ideal for passing / failing test results to roll up visually to the requirements, allowing for quick visualization of the requirement status with regards to implementation and verification.


### Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/product-processes/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

Top competitors in this area are traditional tools that do requirements management used by business analysts, managers, and similar personas. Jama Connect and IBM Rational DOORS are two popular tools. Both of these tools offer limited integration with version control systems, making linking to necessary artifacts cumbersome. While these tools may be necessary for complex system level requirement work, we believe that managing requirements within GitLab can offer a much better user experience for individual teams who are not trying to integrate numerous complex systems.

### Analyst landscape
<!-- What analysts and/or thought leaders in the space talking about, and how we stay
relevant from their perspective.-->

We have yet to engage more closely with analysts in this area. As this product category is prioritized for improvements as our Plan product and engineering headcount grows, we expect to engage more with analysts.

### Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

The next step is building out the requirements management structure to achieve [viable maturity](https://gitlab.com/groups/gitlab-org/-/epics/1697).

### Top user issue(s) & Epics
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

| Issue / Epic                                                                                                                      | 👍 |
| -                                                                                                                                 | -  |
| [Multi-level epic and issue relationships tree-view as requirements management](https://gitlab.com/gitlab-org/gitlab/issues/7021) | 12 |
| [Requirements at the Group Level](https://gitlab.com/groups/gitlab-org/-/epics/707)                                               | 9  |
| [Requirements Management: Approve and Baseline](https://gitlab.com/gitlab-org/gitlab/issues/8461)                                 | 4  |


### Top dogfooding issues
<!-- These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding)
the product.-->

Currently, the quality department is [experimenting with requirements management](https://gitlab.com/gitlab-org/quality/team-tasks/issues/187). Further collaboration is necessary to understand their needs and whether or not our implementation is useful for their efforts.


## Related links
* [Requirements Management Initial Discussion](https://youtu.be/Ci3FTjYrwrA)
* [Requirements management MVC speedrun](https://www.youtube.com/watch?v=uSS7oUNSEoU)
* [Satisfy Requirements with CI Testing](https://www.youtube.com/watch?v=4m1mSEb2ywU)

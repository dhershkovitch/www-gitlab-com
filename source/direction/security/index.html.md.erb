---
layout: secure_and_protect_direction
title: Product Section Direction - Security
description: "Security visibility from development to operations to minimize risk"
canonical_path: "/direction/security/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

<p align="center">
    <font size="+2">
        <b>Security visibility from development to operations to minimize risk</b>
    </font>
</p>

<!-- GitLab Security intro. -->

<%= devops_diagram(["Secure","Protect"]) %>

## Section Overview

GitLab Security Direction is provided by the Sec Product Team.

### Groups

The Security Section is made up of two DevOps stages, Secure and Protect, and eight groups supporting the major categories of DevSecOps including:

* Static Analysis - Assess your applications and services by scanning your source code for vulnerabilities and weaknesses.
* Dynamic Analysis - Assess your applications and services while they are running by leveraging the [Review App](https://docs.gitlab.com/ee/ci/review_apps/) available as part of GitLab’s CI/CD functionality.
* Composition Analysis - Assess your applications and services by analyzing dependencies for vulnerabilities and weaknesses, confirming only approved licenses are in use, and scanning your containers for vulnerabilities and weaknesses.
* Fuzz Testing - Assess your applications and services by inputting unexpected, malformed, and/or random data to measure response or stability by monitoring for unexpected behavior or crashes.
* Threat Insights - Holistically view, manage, and reduce potential risks across the entire DevSecOps lifecycle including Security Merge Request Views, Pipeline Security Reports, and Security Dashboards at the Project, Group, and Instance level.
* Container Security - Monitor and protect your cloud-native applications and services by leveraging context-aware knowledge to improve your overall security posture.
* Vulnerability Research - Leverage GitLab research to empower your Secure results by connecting security findings to industry references like [CVE IDs](https://cve.mitre.org).

### Resourcing and Investment

The existing team members for the Sec Section can be found in the links below:

* [Development](https://about.gitlab.com/company/team/?department=sec-section)
* [User Experience](https://about.gitlab.com/company/team/?department=secure-ux-team)
* [Product Management](https://about.gitlab.com/company/team/?department=sec-pm-team)
* [Quality Engineering](https://about.gitlab.com/company/team/?department=secure-enablement-qe-team)

## 3 Year Section Themes

### Security Is A Team Effort

Everyone benefits when security is a team effort as testing happens regularly, issues are found earlier, and code shipped is more secure.  To make this possible, security must be approachable, not overburden teams, and results must be easy to interpret.  Security testing tools and processes must be [adapted to your developers](https://www.gartner.com/document/code/450792?ref=ddisp&refval=450792) (and not the other way around).  This means bringing security into the workflow of your developers such that they can stay within their context without having unnecessary steps added to their daily work.  Furthermore, results provided as security findings must be presented in a way that they can be interpreted without needing a PhD in cybersecurity.  This includes providing enough detail to begin identifying the root cause (including identifying the section of code causing the security finding) and suggesting remediation steps (including [automatic remediation](#automatic-remediation)) as well as pointing to industry standards related to the security finding.  By implementing an integrated DevSecOps lifecycle with actionable results, security becomes everyone’s responsibility.

As examples, GitLab will provide:
* Security findings within the merge request view allowing developers to stay within their context.
* Security gates enabling the ability to require approval to merge when security findings are above a pre-set severity.
* Industry references with security findings to aid in root cause analysis and remediation.
* [Automatic remediation](#automatic-remediation), where applicable, enabling faster resolution of security findings.
* Summary views, or dashboards, showing the overall health of the project, group, and instance enabling security teams to contribute and interact with development teams.
* Historical reporting for security findings such that root cause can be identified and addressed, preventing recurring issues.

### Shift Left. No, More Left Than That.

The “shift left” approach is not a new concept within software testing and DevOps best practices.  It is commonly thought of when discussing the [DevSecOps lifecycle](https://www.devsecops.org/blog/2016/5/20/-security). This usually includes security testing earlier in the software development lifecycle with the goal of identifying security vulnerabilities and weaknesses prior to shipping code to operations.  Today’s techniques include static application security testing (SAST), dynamic application security testing (DAST), interactive application security testing (IAST), dependency scanning, and license compliance.  The continuation of the “shift left” approach requires a harder shift left, bringing security testing as close as possible to the developer.  This enables earlier detection of security vulnerabilities and weaknesses, thus lowering the cost of remediation (as well as reducing work for the entire team as security findings are addressed prior to reaching the QA and security teams).

As examples, GitLab will provide:
* Static analysis beyond traditional SAST functionality, enabling more ways to [white-box test](https://en.wikipedia.org/wiki/White-box_testing) source code.
* Expanded identification functionality, enabling secret detection beyond API tokens, passwords, and cryptographic keys with improved secrets management leveraging cloud-native technologies.
* Real-time feedback to developers via integrated development environments (IDEs), enabling developers to write secure code prior to code commit.

### Shift Right. Yes, Right. Right Into Operations.

Security testing doesn’t stop once code is shipped.  New vulnerabilities, security weaknesses and, attacker techniques are constantly discovered, leaving operations and their associated applications and services open to being compromised.  Also, as organizations continue to shift to the cloud and employ cloud-native strategies, new attack surfaces are exposed that did not exist within the traditional data center.  These include items like cloud storage permissions and unwanted network services. Applications, services, and their associated cloud-native infrastructure need to be assessed just as a user (or an attacker) would interact with them.  This means performing the same tasks that an attacker would perform including reconnaissance, vulnerability assessment, and penetration testing.  Implementing a continuous assessment strategy of operations is needed to provide full visibility into all potential risk.

As examples, GitLab will provide:
* Dynamic analysis categories, including DAST and DAST API, providing the ability to assess the operations side of the DevOps lifecycle.
* New categories purposefully designed to assess the operations network including automatic penetration testing and vulnerability assessment functionality.
* Cloud-native assessment capabilities allowing users to truly understand their attack surface.
* Active monitoring of public key infrastructure (PKI), providing full visibility into the cryptographic health of your cloud-native environment (and associated containers).

### Blur the line between development and security operations

One of the key advantages to GitLab being a single application for the entire DevOps lifecycle is that
all of our stages are tightly integrated. This empowers GitLab to both provide information into
other stages, such as Plan, as well as to receive information from other stages, such as Monitor.

GitLab will leverage knowledge from the Secure Stage and provide virtual patching of security policies within Protect Categories.
Additionally, Protect will eventually leverage Secure scanning capabilities to provide on-going scanning of applications after
they have been deployed to production.  This allows for detection of any new threat vectors or vulnerabilities that were published
after the original push to production.

Not only does shifting left and acting on results earlier give your apps better security, it helps
[enable collaboration](/handbook/product/product-principles/#enabling-collaboration) with everyone at your company.
We believe that security is everyone&apos;s responsibility and that [everyone can contribute](/company/strategy/#mission).
Informing other stages is a powerful way to do this.

As examples, GitLab will provide:
* Suggested code corrections or package updates to fix vulnerabilities at the source
* Suggested changes to access controls or permissions to better follow the [Principle of Least Privilege](https://en.wikipedia.org/wiki/Principle_of_least_privilege)

### Cloud native first

GitLab will focus first on providing reactive security solutions that are cloud native.  Whenever possible, GitLab capabilities will be cloud platform agnostic, including support for self-hosted cloud native environments.

For example, GitLab&apos;s current Protect capabilities leverage numerous open source cloud-native projects that integrate cleanly with Kubernetes to provide additional security in containerized environments.  Any future expansion of security capabilities will come after the cloud native capabilities are mature.

### Improve OSS security standards

GitLab will leverage OSS security projects to build our solutions.  GitLab will contribute improvements to these projects upstream helping everyone be more secure.

For example, GitLab recently added a [Policy Audit Mode](https://cilium.io/blog/2020/06/22/cilium-18/#policy-audit-mode) to the upstream open source [Cilium](https://cilium.io/) project to allow users to test their policies before enforcing them.

## 1 Year Plan

### What We Are Currently Working On

The Secure team is actively working to bring world class security to DevSecOps and the following outlines where we are currently investing our efforts:
* **Application Security Testing (AST) Leadership** - We will take a leadership position within the Application Security Testing (AST) market.  This will be accomplished by focusing on moving [Static Analysis (SAST)](https://about.gitlab.com/direction/secure/#sast), [Dynamic Analysis (DAST)](https://about.gitlab.com/direction/secure/#dast), and [Dependency Scanning](https://about.gitlab.com/direction/secure/#dependency-scanning) categories to [Complete](https://about.gitlab.com/direction/maturity/) maturity as well as moving [Vulnerability Management](https://about.gitlab.com/direction/secure/vulnerability_management/) to [Viable](https://about.gitlab.com/direction/maturity/) maturity and introducing a [Fuzz Testing](https://about.gitlab.com/direction/secure/#fuzz-testing) category and bringing it to [Viable](https://about.gitlab.com/direction/maturity/) maturity.
* **Dogfooding** - We will [“practice what we preach”](https://www.dictionary.com/browse/practice-what-you-preach), including leveraging Secure Categories in all things GitLab does.  This tight circle will provide immediate feedback and increase our rate of learning.
* **Security for everyone** - In order to make security accessible to everyone across the DevOps lifecycle, we will bring all Secure OSS scanners to Core (self-hosted) / Free (GitLab.com).
* **Security Orchestration** - Provide a unified user experience for viewing, triaging, and resolving alerts from cloud-native environments while also enabling security policy management across development, staging, and production.

### What's Next for Sec

To meet our [audacious goals](https://about.gitlab.com/company/strategy/#big-hairy-audacious-goal), the Secure Stage will focus on the following over the next 12 months:
* **Historical trending** - Provide a focus on identifying patterns in security findings with a goal of helping everyone code securely.  Make recommendations on remediation with a goal of providing [automatic remediation](#automatic-remediation) wherever possible.
* **Machine learning** - Machine learning (ML) will be leveraged, as part of static analysis, to identify insecure coding practices and help developers write more secure code.  This will be opt-in and will enable the power of the GitLab global community.

### What We're Not Doing

The following will NOT be a focus over the next 12 months:
* **Protocol fuzzing** - Fuzzing the entire application technology stack is part of Secure’s 3 Year Strategy; however, we will focus on applications and APIs first.  The shift to protocol fuzzing will occur as Viable and Complete [maturities](https://about.gitlab.com/direction/maturity/#legend) are achieved on DAST and API security testing.
* **Security services** - The cybersecurity staffing shortage [continues to grow](https://www.forbes.com/sites/martenmickos/2019/06/19/the-cybersecurity-skills-gap-wont-be-solved-in-a-classroom/#79a380dd1c30) with no solvable solution yet defined.  To solve this issue, organizations have been relying on security services to fill this gap in their security processes.  As part of Secure’s 3 Year Strategy, we want to address this for the GitLab community by offering cybersecurity augmentation powered by GitLab Secure categories.
* **SIEM functionality** - Security Information and Event Management (SIEM) solutions are a common tool leveraged by IT Ops and SecOps organizations to monitor for events within their production environments. The Protect stage will support exporting security events to external SIEM solutions, including the Monitor stage, as well as Monitor on-call functionality.
* **Non-cloud native environments** - Enterprises are undergoing cloud-native transformations shifting from traditional data centers to public cloud environments leveraging technologies like Kubernetes. Protect will focus on meeting enterprises where they are going and not focus on where they are coming from.

## Target audience

GitLab identifies who our DevSecOps application is built for utilizing the following categorization. We list our view of who we will support when in priority order.
* 🟩- Targeted with strong support
* 🟨- Targeted but incomplete support
* ⬜️- Not targeted but might find value

### Today
To capitalize on the [opportunities](#opportunities) listed above, the Sec Section has features that make it useful to the following personas today.
1. 🟩 Developers / Development Teams
1. 🟩 Security Teams
1. 🟨 SecOps Teams
1. 🟨 QA engineers / QA Teams
1. ⬜️ Security Consultants

### Medium Term (1-2 years)
As we execute our [3 year strategy](#3-year-strategy), our medium term (1-2 year) goal is to provide a single DevSecOps application that enables collaboration between developers, security teams, SecOps teams, and QA Teams.
1. 🟩 Developers / Development Teams
1. 🟩 Security Teams
1. 🟩 SecOps Teams
1. 🟩 QA engineers / QA Teams
1. 🟨️ Security Consultants

## Key Themes
- [Auto remediation](https://gitlab.com/groups/gitlab-org/-/epics/759)
- [Static application security testing (SAST)](https://gitlab.com/groups/gitlab-org/-/epics/527)
- [Secret detection](https://gitlab.com/groups/gitlab-org/-/epics/675)
- [Bill of materials (dependency list)](https://gitlab.com/groups/gitlab-org/-/epics/858)
- [Security dashboard](https://gitlab.com/groups/gitlab-org/-/epics/275)

## Stages and Categories

The Sec section is composed of two stages, each of which contains several categories. Each stage has an overall
strategy statement below, aligned to the themes for Sec. Each category within each stage has a dedicated direction page
plus optional documentation, marketing pages, and other materials linked below.

<%= partial("direction/categories", :locals => { :stageKey => "secure" }) %>

<%= partial("direction/categories", :locals => { :stageKey => "protect" }) %>

<p align="center">
    <i><br>
    Last Reviewed: 2020-11-18<br>
    Last Updated: 2020-11-18
    </i>
</p>